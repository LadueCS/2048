# 2048

## What is this?

It's 2048, written in Python by the Ladue High School Computer Science Club. It's still in its early stages, so expect bugs!

## Getting started

### Download and run

Download or clone this repo, then run `python3 main.py`.

If Python complains about `termcolor`, install it with `pip install termcolor`.

### Run using pipx (recommended)

`pipx run 2048-py`. That's it!

### Install using `pip`

Install with `pip install 2048-py`.

Run `2048-py` to play the game.

### Get it from the AUR

On Arch Linux, get the `2048-py` package from the AUR.

Run `2048-py` to play the game.

### Get it from the FreeBSD package repository

On FreeBSD, install `2048-py` with `pkg install -g py*-2048-py`.

Run `2048-py` to play the game.
